CC = clang
AR = ar -r
RANLIB = ranlib
MACROS = -DNO_ATOMIC -DSSE2 #-DNICS_INT64 #-DNO_EXTENSION
ARCH = -msse2
CFLAGS = -fpic -c -O2 -Wall -Wextra $(ARCH) $(MACROS)
LFLAGS = -shared 
