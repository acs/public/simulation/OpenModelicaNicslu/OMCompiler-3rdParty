# ---------------------------------------------------------------
# $Revision: 1 $
# $Date: 2018-03-28 13:29:20 +0100 (Wed, 28 Mar 2018) $
# ---------------------------------------------------------------
# Programmer:  Lennart M. Schumacher
# ---------------------------------------------------------------
# Copyright (c) 2013, The Regents of the University of California.
# Produced at the Lawrence Livermore National Laboratory.
# All rights reserved.
# For details, see the LICENSE file.
# ---------------------------------------------------------------
# Find NICSLU library.
# 

set(PRE "lib")
IF(WIN32)
  set(POST ".lib" ".dll")
  if(IS_MINGW32 OR IS_MINGW64)
    set(POST ".a" ".dll")
  endif(IS_MINGW32 OR IS_MINGW64)
else(WIN32)
  set(POST ".so")
endif(WIN32)

if (NICSLU_LIBRARY)
    set(temp_NICSLU_LIBRARY_DIR ${NICSLU_LIBRARY_DIR})
    get_filename_component(NICSLU_LIBRARY_DIR ${NICSLU_LIBRARY} PATH)
	if(NOT NICSLU_LIBRARY_DIR)
	  set(NICSLU_LIBRARY_DIR ${temp_NICSLU_LIBRARY_DIR})
	endif(NOT NICSLU_LIBRARY_DIR)
else (NICSLU_LIBRARY)
    # SGS TODO Assumption here that all of SparseSuite is in the same dir
    # SGS TODO Not sure why this is convoluted.
    set(NICSLU_LIBRARY_NAME nicslu)
    
    # find library path using potential names for static and/or shared libs
    set(temp_NICSLU_LIBRARY_DIR ${NICSLU_LIBRARY_DIR})
    unset(NICSLU_LIBRARY_DIR CACHE)  
    find_path(NICSLU_LIBRARY_DIR
        NAMES ${PRE}${NICSLU_LIBRARY_NAME}${POST}
        PATHS ${temp_NICSLU_LIBRARY_DIR}
        )
  
    MESSAGE("NICSLU INC3:")
    MESSAGE(${NICSLU_INCLUDE_DIR})
    MESSAGE("NICSLU LIB3:")
    MESSAGE(${NICSLU_LIBRARY_DIR})
    
	mark_as_advanced(NICSLU_LIBRARY)

    FIND_LIBRARY( NICSLU_LIBRARY ${PRE}nicslu${POST} ${NICSLU_LIBRARY_DIR} NO_DEFAULT_PATH)
endif (NICSLU_LIBRARY)

if (NICSLU_UTIL)
	set(temp_NICSLU_UTIL_DIR ${NICSLU_LIBRARY_DIR})
   get_filename_component(NICSLU_LIBRARY_DIR ${NICSLU_UTIL} PATH)
	   if(NOT NICSLU_UTIL_DIR)
		   set(NICSLU_LIBRARY_DIR ${temp_NICSLU_UTIL_DIR})
      	   endif(NOT NICSLU_UTIL_DIR)
else (NICSLU_UTIL)
   set(NICSLU_UTIL_NAME nicslu_util)
   set(temp_NICSLU_UTIL_DIR ${NICSLU_LIBRARY_DIR})
   unset(NICSLU_UTIL_DIR CACHE)
   find_path(NICSLU_UTIL_DIR
       NAMES ${PRE}${NICSLU_UTIL_NAME}${POST}
       PATHS ${temp_NICSLU_LIBRARY_DIR}
       )

    MESSAGE("NICSLU_UTIL LIB3:")
    MESSAGE(${NICSLU_LIBRARY_DIR})

    	mark_as_advanced(NICSLU_UTIL)

	FIND_LIBRARY( NICSLU_UTIL ${PRE}nicsluutil${POST} ${NICSLU_LIBRARY_DIR} NO_DEFAULT_PATH)
endif (NICSLU_UTIL)

MESSAGE("NICSLU LIBS: " ${NICSLU_LIBRARY} ${NICSLU_UTIL})

set(NICSLU_LIBRARIES ${NICSLU_LIBRARY} ${NICSLU_UTIL})
