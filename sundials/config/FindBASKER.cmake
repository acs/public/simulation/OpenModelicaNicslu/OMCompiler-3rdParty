# ---------------------------------------------------------------
# $Revision: 1 $
# $Date: 2019-03-11 13:34:00 +0100 (Mon, 11 Mar 2019) $
# ---------------------------------------------------------------
# Programmer:  Lennart M. Schumacher
# ---------------------------------------------------------------
# Copyright (c) 2013, The Regents of the University of California.
# Produced at the Lawrence Livermore National Laboratory.
# All rights reserved.
# For details, see the LICENSE file.
# ---------------------------------------------------------------
# Find BASKER library.
# 

set(PRE "lib")
IF(WIN32)
  set(POST ".lib" ".dll")
  if(IS_MINGW32 OR IS_MINGW64)
    set(POST ".a" ".dll")
  endif(IS_MINGW32 OR IS_MINGW64)
else(WIN32)
  set(POST ".so")
endif(WIN32)

if (BASKER_LIBRARY)
    set(temp_BASKER_LIBRARY_DIR ${BASKER_LIBRARY_DIR})
    get_filename_component(BASKER_LIBRARY_DIR ${BASKER_LIBRARY} PATH)
	if(NOT BASKER_LIBRARY_DIR)
	  set(BASKER_LIBRARY_DIR ${temp_BASKER_LIBRARY_DIR})
	endif(NOT BASKER_LIBRARY_DIR)
else (BASKER_LIBRARY)
    set(BASKER_LIBRARY_NAME shylu_nodebasker)
    
    # find library path using potential names for static and/or shared libs
    set(temp_BASKER_LIBRARY_DIR ${BASKER_LIBRARY_DIR})
    unset(BASKER_LIBRARY_DIR CACHE)  
    find_path(BASKER_LIBRARY_DIR
        NAMES ${PRE}${BASKER_LIBRARY_NAME}${POST}
        PATHS ${temp_BASKER_LIBRARY_DIR}
        )
  
    MESSAGE("BASKER INC3:")
    MESSAGE(${BASKER_INCLUDE_DIR})
    MESSAGE("BASKER LIB3:")
    MESSAGE(${BASKER_LIBRARY_DIR})
    
    mark_as_advanced(BASKER_LIBRARY)

    FIND_LIBRARY( BASKER_LIBRARY ${PRE}shylu_nodebasker${POST} ${BASKER_LIBRARY_DIR} NO_DEFAULT_PATH)
endif (BASKER_LIBRARY)

if (BASKER_UTIL)
	set(temp_BASKER_UTIL_DIR ${BASKER_LIBRARY_DIR})
	get_filename_component(BASKER_LIBRARY_DIR ${BASKER_UTIL} PATH)
	if(NOT BASKER_UTIL_DIR)
		set(BASKER_LIBRARY_DIR ${temp_BASKER_UTIL_DIR})
	endif(NOT BASKER_UTIL_DIR)
else (BASKER_UTIL)
    set(BASKER_UTIL_NAME kokkoscore)
    set(temp_BASKER_UTIL_DIR ${BASKER_LIBRARY_DIR})
    unset(BASKER_UTIL_DIR CACHE)
    find_path(BASKER_UTIL_DIR
	    NAMES ${PRE}${BASKER_UTIL_NAME}${POST}
        PATHS ${temp_BASKER_LIBRARY_DIR}
        )

    MESSAGE("BASKER_UTIL LIB3:")
    MESSAGE(${BASKER_UTIL_DIR})

    mark_as_advanced(BASKER_UTIL)

    FIND_LIBRARY( BASKER_UTIL ${PRE}kokkoscore${POST} ${BASKER_LIBRARY_DIR} NO_DEFAULT_PATH)
endif (BASKER_UTIL)

if (BASKER_SS)
	set(temp_BASKER_SS_DIR ${BASKER_LIBRARY_DIR})
	get_filename_component(BASKER_LIBRARY_DIR ${BASKER_SS} PATH)
	if(NOT BASKER_SS_DIR)
		set(BASKER_SS_DIR ${temp_BASKER_SS_DIR})
	endif(NOT BASKER_SS_DIR)
else (BASKER_SS)
	set(BASKER_SS_NAME trilonosss)
	set(temp_BASKER_SS_DIR ${BASKER_LIBRARY_DIR})
	unset(BASKER_SS_DIR CACHE)
	find_path(BASKER_SS_DIR
		NAMES ${PRE}${BASKER_SS_NAME}${POST}
	    PATHS ${temp_BASKER_LIBRARY_DIR}
		)

	MESSAGE("BASKER_SS LIB3:")
	MESSAGE(${BASKER_SS_DIR})

	mark_as_advanced(BASKER_SS)

	FIND_LIBRARY( BASKER_SS ${PRE}trilinosss${POST} ${BASKER_LIBRARY_DIR} NO_DEFAULT_PATH)
endif (BASKER_SS)

MESSAGE("BASKER LIBS: " ${BASKER_LIBRARY} ${BASKER_UTIL} ${BASKER_SS})

set(BASKER_LIBRARIES ${BASKER_LIBRARY} ${BASKER_UTIL} ${BASKER_SS})
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_C_FLAGS}")
#set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
