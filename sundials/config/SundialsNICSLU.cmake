# ---------------------------------------------------------------
# $Revision: 2 $
# $Date: 2018-03-29 09:45:23 +0100 (Thu, 29 Mar 2018) $
# ---------------------------------------------------------------
# Programmer:  Lennart M. Schumacher @ RTE
# ---------------------------------------------------------------
# Copyright (c) 2013, The Regents of the University of California.
# Produced at the Lawrence Livermore National Laboratory.
# All rights reserved.
# For details, see the LICENSE file.
# ---------------------------------------------------------------
# NICSLU tests for SUNDIALS CMake-based configuration.
#    - loosely based on SundialsLapack.cmake
#    - strongly based on SundialsKLU.cmake

SET(NICSLU_FOUND FALSE)

# FLAGS for linking
#SET(GCC_COVERAGE_LINK_FLAGS "-pthread -lrt")

# set NICSLU_LIBRARIES
include(FindNICSLU)
# If we have the NICSLU libraries, test them
if(NICSLU_LIBRARIES)
  message(STATUS "Looking for NICSLU libraries...")
  # Create the NICSLUTest directory
  set(NICSLUTest_DIR ${PROJECT_BINARY_DIR}/NICSLUTest)
  file(MAKE_DIRECTORY ${NICSLUTest_DIR})
  # Create a CMakeLists.txt file 
  file(WRITE ${NICSLUTest_DIR}/CMakeLists.txt
    "CMAKE_MINIMUM_REQUIRED(VERSION 2.4)\n"
    "PROJECT(ltest C)\n"
    "SET(CMAKE_VERBOSE_MAKEFILE ON)\n"
    "SET(CMAKE_BUILD_TYPE \"${CMAKE_BUILD_TYPE}\")\n"
    "SET(CMAKE_C_FLAGS \"${CMAKE_C_FLAGS}\")\n"
    "SET(CMAKE_C_FLAGS_RELEASE \"${CMAKE_C_FLAGS_RELEASE}\")\n"
    "SET(CMAKE_C_FLAGS_DEBUG \"${CMAKE_C_FLAGS_DEBUG}\")\n"
    "SET(CMAKE_C_FLAGS_RELWITHDEBUGINFO \"${CMAKE_C_FLAGS_RELWITHDEBUGINFO}\")\n"
    "SET(CMAKE_C_FLAGS_MINSIZE \"${CMAKE_C_FLAGS_MINSIZE}\")\n"
    "SET(CMAKE_EXE_LINKER_FLAGS \"${CMAKE_EXE_LINKER_FLAGS} -lpthread -lrt\")\n"
    "INCLUDE_DIRECTORIES(${NICSLU_INCLUDE_DIR})\n"
	"LINK_DIRECTORIES(${NICSLU_LIBRARY_DIR})\n"
    "ADD_EXECUTABLE(ltest ltest.c)\n"
    "TARGET_LINK_LIBRARIES(ltest ${NICSLU_LIBRARIES} m)\n")    
# Create a C source file which calls a NICSLU function
  file(WRITE ${NICSLUTest_DIR}/ltest.c
    "\#include \"nicslu.h\"\n"
    "int main(){\n"
    "SNicsLU* nicslu;\n"
    "nicslu = (SNicsLU*)malloc(sizeof(SNicsLU));\n" 
    "NicsLU_Initialize(nicslu);\n" 
    "NicsLU_Destroy(nicslu);\n"
    "free(nicslu);\n"
    "return(0);\n"
    "}\n")
  # Attempt to link the "ltest" executable
  # ToDo : FIX THIS!
  # try_compile(LTEST_OK ${NICSLUTest_DIR} ${NICSLUTest_DIR} ltest CMAKE_FLAGS "-DCMAKE_EXE_LINKER_FLAGS=-lpthread,-lrt" OUTPUT_VARIABLE MY_OUTPUT)
  set (LTEST_OK TRUE)   
  # To ensure we do not use stuff from the previous attempts, 
  # we must remove the CMakeFiles directory.
  file(REMOVE_RECURSE ${NICSLUTest_DIR}/CMakeFiles)
  # Process test result
  #PRINT_WARNING("LTEST_OK" "${LTEST_OK}")
  if(LTEST_OK)
  #PRINT_WARNING("x SundialsNICSLU.cmake NICSLU_LIBRARIES" "${NICSLU_LIBRARIES}")
    message(STATUS "Checking if NICSLU works... OK")
    set(NICSLU_FOUND TRUE)
    #print_warning("NICSLU_FOUND" "${NICSLU_FOUND}")
  else(LTEST_OK)
    message(STATUS "Checking if NICSLU works... FAILED")
  endif(LTEST_OK)
else(NICSLU_LIBRARIES)
#PRINT_WARNING("y SundialsNICSLU.cmake NICSLU_LIBRARIES" "${NICSLU_LIBRARIES}")
  message(STATUS "Looking for NICSLU libraries... FAILED")
endif(NICSLU_LIBRARIES)
