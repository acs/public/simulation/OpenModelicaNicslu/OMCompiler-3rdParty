# ---------------------------------------------------------------
# $Revision: 2 $
# $Date: 2019-03-11 13:36:55 +0100 (Mon, 11 Mar 2019) $
# ---------------------------------------------------------------
# Programmer:  Lennart M. Schumacher @ ACS
# ---------------------------------------------------------------
# Copyright (c) 2013, The Regents of the University of California.
# Produced at the Lawrence Livermore National Laboratory.
# All rights reserved.
# For details, see the LICENSE file.
# ---------------------------------------------------------------
# BASKER tests for SUNDIALS CMake-based configuration.
#    - loosely based on SundialsLapack.cmake
#    - strongly based on SundialsKLU.cmake

SET(BASKER_FOUND FALSE)

# set BASKER_LIBRARIES
include(FindBASKER)
# If we have the BASKER libraries, test them
#TODO: fix linking for Basker
if(BASKER_LIBRARIES)
  message(STATUS "Looking for BASKER libraries...")
  enable_language(CXX)
  # Create the BASKERTest directory
  set(BASKERTest_DIR ${PROJECT_BINARY_DIR}/BASKERTest)
  find_package( OpenMP REQUIRED)
  file(MAKE_DIRECTORY ${BASKERTest_DIR})
  # Create a CMakeLists.txt file 
  file(WRITE ${BASKERTest_DIR}/CMakeLists.txt
    "CMAKE_MINIMUM_REQUIRED(VERSION 2.4)\n"
    "PROJECT(ltest CXX)\n"
    "SET(CMAKE_VERBOSE_MAKEFILE ON)\n"
    "SET(CMAKE_BUILD_TYPE \"${CMAKE_BUILD_TYPE}\")\n"
    "SET(CMAKE_CXX_FLAGS \"${CMAKE_CXX_FLAGS} ${OpenMP_C_FLAGS}\")\n"
    "SET(CMAKE_CXX_FLAGS_RELEASE \"${CMAKE_CXX_FLAGS_RELEASE}\")\n"
    "SET(CMAKE_CXX_FLAGS_DEBUG \"${CMAKE_CXX_FLAGS_DEBUG}\")\n"
    "SET(CMAKE_CXX_FLAGS_RELWITHDEBUGINFO \"${CMAKE_CXX_FLAGS_RELWITHDEBUGINFO}\")\n"
    "SET(CMAKE_CXX_FLAGS_MINSIZE \"${CMAKE_CXX_FLAGS_MINSIZE}\")\n"
    "SET(CMAKE_EXE_LINKER_FLAGS \"${CMAKE_EXE_LINKER_FLAGS} -fopenmp -lrt\")\n"
    "INCLUDE_DIRECTORIES(${BASKER_INCLUDE_DIR})\n"
	"LINK_DIRECTORIES(${BASKER_LIBRARY_DIR})\n"
    "ADD_EXECUTABLE(ltest ltest.cpp)\n"
    "TARGET_LINK_LIBRARIES(ltest ${BASKER_LIBRARIES} ${BASKER_UTIL} m)\n")    
# Create a C++ source file which calls a BASKER function
  file(WRITE ${BASKERTest_DIR}/ltest.cpp
    "\#include \"shylubasker_def.hpp\"\n"
    "\#include \"shylubasker_decl.hpp\"\n"
    "typedef double Entry;\n"
    "typedef int Int;\n"
    "typedef void* Exe_Space;\n"
    "int main(){\n"
    "BaskerNS::Basker<Int,Entry,Exe_Space> mybasker;\n"
    "mybasker.Finalize();\n"
    "return(0);\n"
    "}\n")
  # Attempt to link the "ltest" executable
  # ToDo : FIX THIS!
  # try_compile(LTEST_OK ${BASKERTest_DIR} ${BASKERTest_DIR} ltest CMAKE_FLAGS "-DCMAKE_EXE_LINKER_FLAGS=-lpthread,-lrt" OUTPUT_VARIABLE MY_OUTPUT)
  set (LTEST_OK TRUE)   
  # To ensure we do not use stuff from the previous attempts, 
  # we must remove the CMakeFiles directory.
  file(REMOVE_RECURSE ${BASKERTest_DIR}/CMakeFiles)
  # Process test result
  #PRINT_WARNING("LTEST_OK" "${LTEST_OK}")
  if(LTEST_OK)
  #PRINT_WARNING("x SundialsBASKER.cmake BASKER_LIBRARIES" "${BASKER_LIBRARIES}")
    message(STATUS "Checking if BASKER works... OK")
    set(BASKER_FOUND TRUE)
    #print_warning("BASKER_FOUND" "${BASKER_FOUND}")
  else(LTEST_OK)
    message(STATUS "Checking if BASKER works... FAILED")
  endif(LTEST_OK)
else(BASKER_LIBRARIES)
#PRINT_WARNING("y SundialsBASKER.cmake BASKER_LIBRARIES" "${BASKER_LIBRARIES}")
  message(STATUS "Looking for BASKER libraries... FAILED")
endif(BASKER_LIBRARIES)
