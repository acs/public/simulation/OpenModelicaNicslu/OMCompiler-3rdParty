/*
 * -----------------------------------------------------------------
 * $Revision: 1 $
 * $Date: 2018-04-05 13:23:31 +0100 (Thu, 05 Apr 2018) $
 * -----------------------------------------------------------------
 * Programmer(s): Lennart M. Schumacher @ RTE
 * -----------------------------------------------------------------
 * LLNS Copyright Start
 * Copyright (c) 2014, Lawrence Livermore National Security
 * This work was performed under the auspices of the U.S. Department
 * of Energy by Lawrence Livermore National Laboratory in part under
 * Contract W-7405-Eng-48 and in part under Contract DE-AC52-07NA27344.
 * Produced at the Lawrence Livermore National Laboratory.
 * All rights reserved.
 * For details, see the LICENSE file.
 * LLNS Copyright End
 * -----------------------------------------------------------------
 * This is the implementation file for the KINNICSLU linear solver.
 * -----------------------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>

#include <sundials/sundials_math.h>

#include "kinsol/kinsol_nicslu.h"
#include "kinsol_impl.h"
#include "kinsol_sparse_impl.h"
#include "sundials/sundials_nicslu_impl.h"

/* Constants */

#define ONE          RCONST(1.0)
#define TWO          RCONST(2.0)
#define TWOTHIRDS    RCONST(0.6666666666666667)

/* KINNICSLU linit, lsetup, lsolve, and lfree routines */

static int kinNICSLUInit(KINMem kin_mem);
static int kinNICSLUSetup(KINMem kin_mem);
static int kinNICSLUSolve(KINMem kin_mem, N_Vector x, N_Vector b,
               realtype *sJpnorm, realtype *sFdotJp);
static void kinNICSLUFree(KINMem kin_mem);

/*
 * -----------------------------------------------------------------
 * KINNICSLU
 * -----------------------------------------------------------------
 * This routine initializes the memory record and sets various function
 * fields specific to the KINSOL / NICSLU linear solver module.
 * KINNICSLU first calls the existing lfree routine if this is not NULL.
 * Then it sets the kin_linit, kin_lsetup, kin_lsolve, kin_lperf, and
 * kin_lfree fields in (*kin_mem) to be kinNICSLUInit, kinNICSLUSetup,
 * kinNICSLUSolve, NULL, and , respectively.
 * It allocates memory for a structure of type kinNICSLUMemRec and sets
 * the kin_lmem field in (*kin_mem) to the address of this structure.
 * It sets setupNonNull in (*kin_mem) to TRUE.
 * Finally, it allocates memory for NICSLU.
 * The return value is KINSLS_SUCCESS = 0, KINSLS_LMEM_FAIL = -1,
 * or KINSLS_ILL_INPUT = -2.
 *
 * NOTE: The NICSLU linear solver assumes a serial implementation
 *       of the NVECTOR package. Therefore, KINNICSLU will first
 *       test for a compatible N_Vector internal representation
 *       by checking that the function N_VGetArrayPointer exists.
 * -----------------------------------------------------------------
 */

int kinNICSLU(void *kin_mem_v, int n, int nnz)
{
  KINMem kin_mem;
  KINSlsMem kinsls_mem;
  SNicsLU* nicslu_data;
  int flag;

  /* Return immediately if kin_mem is NULL. */
  if (kin_mem_v == NULL) {
    KINProcessError(NULL, KINSLS_MEM_NULL, "KINSLS", "kinNICSLU",
            MSGSP_KINMEM_NULL);
    return(KINSLS_MEM_NULL);
  }
  kin_mem = (KINMem) kin_mem_v;

  /* Test if the NVECTOR package is compatible with the Direct solver */
  if (kin_mem->kin_vtemp1->ops->nvgetarraypointer == NULL) {
    KINProcessError(kin_mem, KINSLS_ILL_INPUT, "KINSLS", "kinNICSLU",
            MSGSP_BAD_NVECTOR);
    return(KINSLS_ILL_INPUT);
  }

  if (kin_mem->kin_lfree != NULL) kin_mem->kin_lfree(kin_mem);

  /* Set five main function fields in kin_mem. */
  kin_mem->kin_linit  = kinNICSLUInit;
  kin_mem->kin_lsetup = kinNICSLUSetup;
  kin_mem->kin_lsolve = kinNICSLUSolve;
  kin_mem->kin_lfree  = kinNICSLUFree;

  /* Get memory for kinSlsMemRec. */
  kinsls_mem = (KINSlsMem) malloc(sizeof(struct KINSlsMemRec));
  if (kinsls_mem == NULL) {
    KINProcessError(kin_mem, KINSLS_MEM_FAIL, "KINSLS", "kinNICSLU",
            MSGSP_MEM_FAIL);
    return(KINSLS_MEM_FAIL);
  }

  /* Get memory for NICSLUData. */
  nicslu_data = (SNicsLU*)malloc(sizeof(struct tagSNicsLU));
  if (nicslu_data == NULL) {
    KINProcessError(kin_mem, KINSLS_MEM_FAIL, "KINSLS", "kinNICSLU",
            MSGSP_MEM_FAIL);
    return(KINSLS_MEM_FAIL);
  }

  kin_mem->kin_setupNonNull = TRUE;

  /* Set default Jacobian routine and Jacobian data */
  kinsls_mem->s_jaceval = NULL;
  kinsls_mem->s_jacdata = kin_mem->kin_user_data;

  /* Allocate memory for the sparse Jacobian */
  kinsls_mem->s_JacMat = NewSparseMat(n, n, nnz);
  if (kinsls_mem->s_JacMat == NULL) {
    KINProcessError(kin_mem, KINSLS_MEM_FAIL, "KINSLS", "kinNICSLU",
            MSGSP_MEM_FAIL);
    return(KINSLS_MEM_FAIL);
  }

  flag = NicsLU_Initialize(nicslu_data);

  /* This is a direct linear solver */
  kin_mem->kin_inexact_ls = FALSE;

  /* Attach linear solver memory to the nonlinear solver memory */
  kinsls_mem->s_solver_data = (void *) nicslu_data;
  kin_mem->kin_lmem = kinsls_mem;

  kinsls_mem->s_last_flag = KINSLS_SUCCESS;

  return(KINSLS_SUCCESS);
}

/*
 * -----------------------------------------------------------------
 * kinNICSLUReInit
 * -----------------------------------------------------------------
 * This routine reinitializes memory and flags for a new factorization
 * (symbolic and numeric) to be conducted at the next solver setup
 * call.  This routine is useful in the cases where the number of nonzeroes
 * has changed or if the structure of the linear system has changed
 * which would require a new symbolic (and numeric factorization).
 *
 * The reinit_type argumenmt governs the level of reinitialization:
 *
 * reinit_type = 1: The Jacobian matrix will be destroyed and
 *                  a new one will be allocated based on the nnz
 *                  value passed to this call. New symbolic and
 *                  numeric factorizations will be completed at the next
 *                  solver setup.
 *
 * reinit_type = 2: Only symbolic and numeric factorizations will be
 *                  completed.  It is assumed that the Jacobian size
 *                  has not exceeded the size of nnz given in the prior
 *                  call to KINNICSLU.
 *
 * This routine assumes no other changes to solver use are necessary.
 *
 * The return value is KINSLS_SUCCESS = 0, KINSLS_MEM_NULL = -1,
 * KINSLS_LMEM_NULL = -2, KINSLS_ILL_INPUT = -3, or KINSLS_MEM_FAIL = -4.
 *
 * -----------------------------------------------------------------
 */

int kinNICSLUReInit(void *kin_mem_v, int n, int nnz, int reinit_type)
{
  KINMem kin_mem;
  KINSlsMem kinsls_mem;
  SNicsLU* nicslu_data;
  SlsMat JacMat;

  /* Return immediately if kin_mem is NULL. */
  if (kin_mem_v == NULL) {
    KINProcessError(NULL, KINSLS_MEM_NULL, "KINSLS", "kinNICSLUReInit",
            MSGSP_KINMEM_NULL);
    return(KINSLS_MEM_NULL);
  }
  kin_mem = (KINMem) kin_mem_v;

  /* Return immediately if kin_lmem is NULL. */
  if (kin_mem->kin_lmem == NULL) {
    KINProcessError(NULL, KINSLS_LMEM_NULL, "KINSLS", "kinNICSLUReInit",
            MSGSP_LMEM_NULL);
    return(KINSLS_LMEM_NULL);
  }
  kinsls_mem = (KINSlsMem) (kin_mem->kin_lmem);
  nicslu_data = (SNicsLU*) kinsls_mem->s_solver_data;

  /* Return if reinit_type is not valid */
  if ((reinit_type != 1) && (reinit_type != 2)) {
    KINProcessError(NULL, KINSLS_ILL_INPUT, "KINSLS", "kinNICSLUReInit",
            MSGSP_ILL_INPUT);
    return(KINSLS_ILL_INPUT);
  }

  JacMat = kinsls_mem->s_JacMat;

  if (reinit_type == 1) {

    /* Destroy previous Jacobian information */
    if (kinsls_mem->s_JacMat) {
      DestroySparseMat(kinsls_mem->s_JacMat);
    }

    /* Allocate memory for the sparse Jacobian */
    kinsls_mem->s_JacMat = NewSparseMat(n, n, nnz);
    if (kinsls_mem->s_JacMat == NULL) {
      KINProcessError(kin_mem, KINSLS_MEM_FAIL, "KINSLS", "KINNICSLU",
            MSGSP_MEM_FAIL);
      return(KINSLS_MEM_FAIL);
    }
  }

  /* Free the prior factorazation and reset for first factorization */
  /*if( klu_data->s_Symbolic != NULL)
    klu_free_symbolic(&(klu_data->s_Symbolic), &(klu_data->s_Common));
  if( klu_data->s_Numeric != NULL)
    klu_free_numeric(&(klu_data->s_Numeric), &(klu_data->s_Common));*/
  NicsLU_Destroy(nicslu_data);
  //free(nicslu_data);
  kinsls_mem->s_first_factorize = 1;

  kinsls_mem->s_last_flag = KINSLS_SUCCESS;

  return(0);
}

/*
 * -----------------------------------------------------------------
 * KINNICSLU interface functions
 * -----------------------------------------------------------------
 */

/*
  This routine does remaining initializations specific to the KINNICSLU
  linear solver module.
  It returns 0 if successful.
*/

static int kinNICSLUInit(KINMem kin_mem)
{
  KINSlsMem kinsls_mem;
  kinsls_mem = (KINSlsMem)kin_mem->kin_lmem;

  kinsls_mem->s_solver_data = (SNicsLU*) malloc(sizeof(SNicsLU));
  NicsLU_Initialize(kinsls_mem->s_solver_data);

  kinsls_mem->s_nje = 0;
  kinsls_mem->s_first_factorize = 1;

  kinsls_mem->s_last_flag = 0;
  return(0);
}

/*
  This routine does the setup operations for the KINNICSLU linear
  solver module.  It calls the Jacobian evaluation routine,
  updates counters, and calls the LU factorization routine.
  The return value is either
     KINSLS_SUCCESS = 0  if successful,
     +1  if the jac routine failed recoverably or the
         LU factorization failed, or
     -1  if the jac routine failed unrecoverably.
*/

static int kinNICSLUSetup(KINMem kin_mem)
{
  int retval;
  KINSlsMem kinsls_mem;
  KINSlsSparseJacFn jaceval;
  SNicsLU* nicslu_data;
  SlsMat JacMat;
  void *jacdata;
  realtype uround_twothirds;
  int num; // Number of THREADS
  static int (*performFactorization)(SNicsLU *) = NULL;
  static int (*performReFactorization)(SNicsLU *, real__t *) = NULL;


  uround_twothirds = SUNRpowerR(kin_mem->kin_uround,TWOTHIRDS);

  kinsls_mem = (KINSlsMem) (kin_mem->kin_lmem);

  nicslu_data = (SNicsLU*) kinsls_mem->s_solver_data;

  jaceval = kinsls_mem->s_jaceval;
  jacdata = kinsls_mem->s_jacdata;
  JacMat = kinsls_mem->s_JacMat;

  /* Check that Jacobian eval routine is set */
  if (jaceval == NULL) {
    KINProcessError(kin_mem, KINSLS_JAC_NOSET, "KINSLS", "kinNICSLUSetup",
            MSGSP_JAC_NOSET);
    free(kinsls_mem); kinsls_mem = NULL;
    return(KINSLS_JAC_NOSET);
  }

  /* Increment nje counter and call Jacobian eval routine. */
  kinsls_mem->s_nje++;
  retval = jaceval(kin_mem->kin_uu, kin_mem->kin_fval, JacMat, jacdata,
           kin_mem->kin_vtemp1, kin_mem->kin_vtemp2);

  if (retval < 0) {
    KINProcessError(kin_mem, KINSLS_JACFUNC_UNRECVR, "KINSLS",
            "kinNICSLUSetup", MSGSP_JACFUNC_FAILED);
    kinsls_mem->s_last_flag = KINSLS_JACFUNC_UNRECVR;
    return(KINSLS_JACFUNC_UNRECVR);
  }
  if (retval == 1) {
    kinsls_mem->s_first_factorize = 1;
  }
  if (retval > 1) {
    kinsls_mem->s_last_flag = KINSLS_JACFUNC_RECVR;
    return(+1);
  }

  uint__t n = JacMat->N;
  uint__t nnz = JacMat->NNZ;
  uint__t *ai = JacMat->rowvals;
  uint__t *ap = JacMat->colptrs;
  real__t *ax = JacMat->data;

  if (kinsls_mem->s_first_factorize) {
      /* ------------------------------------------------------------
         Get the symbolic factorization
         ------------------------------------------------------------*/

      if (nicslu_data == NULL) {
        KINProcessError(kin_mem, KINSLS_PACKAGE_FAIL, "KINSLS", "kinNICSLUSetup",
                        MSGSP_JAC_NOSET);
        return (+1);
      }

      NicsLU_CreateMatrix(nicslu_data, n, nnz, ax, ai, ap);
      nicslu_data->cfgf[0] = .9;
      nicslu_data->cfgi[0] = 1;

      int ret = NicsLU_Analyze(nicslu_data);
      char *numth = getenv("THREADS");
      if (numth != NULL && atoi(numth) > 1) {
        ret = NicsLU_CreateScheduler(nicslu_data);
        printf("suggestion: %s.\n", ret == 0 ? "parallel" : "sequential");
        num = atoi(numth);
        ret = NicsLU_CreateThreads(nicslu_data, num, TRUE);
        ret = NicsLU_BindThreads(nicslu_data, FALSE);
        performFactorization = &NicsLU_Factorize_MT;
        performReFactorization = &NicsLU_ReFactorize_MT;
        if (ret == NICS_OK)
          printf("%d threads created\n", num);
      } else {
        performFactorization = &NicsLU_Factorize;
        performReFactorization = &NicsLU_ReFactorize;
        num = 1;
      }

      if (ret != NICS_OK) {
        KINProcessError(kin_mem, KINSLS_PACKAGE_FAIL, "KINSLS", "kinNICSLUSetup",
                        MSGSP_PACKAGE_FAIL);
        return (KINSLS_PACKAGE_FAIL);
      }

      /* ------------------------------------------------------------
        Compute the LU factorization of  the Jacobian.
        ------------------------------------------------------------*/
      performFactorization(nicslu_data);
      kinsls_mem->s_first_factorize = 0;

  }
  else {
      performReFactorization(nicslu_data, ax);

      /*-----------------------------------------------------------
        Check if a cheap estimate of the reciprocal of the condition
        number is getting too small.  If so, delete
        the prior numeric factorization and recompute it.
        -----------------------------------------------------------*/
      real__t *cond;
      NicsLU_ConditionNumber(nicslu_data, cond);

      /* Condition number may be getting large.
      Compute more accurate estimate */

      if ((nicslu_data->stat[6]) > (1.0 / uround_twothirds)) {

        /* More accurate estimate also says condition number is
           large, so recompute the numeric factorization */

        printf("Condition number too large!\n");
        performFactorization(nicslu_data);
      }
    }

  kinsls_mem->s_last_flag = KINSLS_SUCCESS;

  return(0);
}

/*
  This routine handles the solve operation for the KINNICSLU linear
  solver module.  It calls the KLU solve routine,
  then returns KINSLS_SUCCESS = 0.
*/

static int kinNICSLUSolve(KINMem kin_mem, N_Vector x, N_Vector b,
               realtype *sJpnorm, realtype *sFdotJp)
{
  int flag;
  KINSlsMem kinsls_mem;
  SNicsLU* nicslu_data;
  SlsMat JacMat;
  realtype *xd;

  kinsls_mem = (KINSlsMem) kin_mem->kin_lmem;
  JacMat = kinsls_mem->s_JacMat;

  nicslu_data = (SNicsLU*) kinsls_mem->s_solver_data;

  /* Copy the right-hand side into x */
  N_VScale(ONE, b, x);
  xd = N_VGetArrayPointer(x);

  /* Call KLU to solve the linear system */
  flag = NicsLU_Solve(nicslu_data, xd);

  if (flag != NICS_OK) {
    KINProcessError(kin_mem, KINSLS_PACKAGE_FAIL, "KINSLS", "kinNICSLUSolve",
            MSGSP_PACKAGE_FAIL);
    return(KINSLS_PACKAGE_FAIL);
  }

  /* Compute the term sFdotJp for use in the linesearch routine.
     This term is subsequently corrected if the step is reduced by
     constraints or the linesearch.

     sFdotJp is the dot product of the scaled f vector and the scaled
     vector J*p, where the scaling uses fscale.                            */

  N_VProd(b, kin_mem->kin_fscale, b);
  N_VProd(b, kin_mem->kin_fscale, b);
  *sFdotJp = N_VDotProd(kin_mem->kin_fval, b);

  kinsls_mem->s_last_flag = KINSLS_SUCCESS;
  return(KINSLS_SUCCESS);
}

/*
  This routine frees memory specific to the KINNICSLU linear solver.
*/

static void kinNICSLUFree(KINMem kin_mem)
{
  KINSlsMem kinsls_mem;
  SNicsLU* nicslu_data;

  kinsls_mem = (KINSlsMem) kin_mem->kin_lmem;
  nicslu_data = (SNicsLU*) kinsls_mem->s_solver_data;
  NicsLU_Destroy(nicslu_data);

  if (kinsls_mem->s_JacMat) {
    DestroySparseMat(kinsls_mem->s_JacMat);
    kinsls_mem->s_JacMat = NULL;
  }

  free(nicslu_data);
  free(kin_mem->kin_lmem);
}
