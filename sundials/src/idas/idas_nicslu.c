#include <stdio.h>
#include <stdlib.h>

#include "idas/idas_nicslu.h"
#include "idas_impl.h"
#include "idas_sparse_impl.h"
#include "sundials/sundials_math.h"
#include "sundials/sundials_nicslu_impl.h"

static int IDANICSLUInit(IDAMem IDA_mem);

static int IDANICSLUSetup(IDAMem IDA_mem, N_Vector yyp, N_Vector ypp,
                          N_Vector rrp, N_Vector tmp1, N_Vector tmp2,
                          N_Vector tmp3);

static int IDANICSLUSolve(IDAMem IDA_mem, N_Vector b, N_Vector weight,
                          N_Vector ycur, N_Vector ypcur, N_Vector rrcur);

static int IDANICSLUFree(IDAMem IDA_mem);

/* IDANICSLU lfreeB function */

static void IDANICSLUFreeB(IDABMem IDAB_mem);

#define ONE RCONST(1.0)
#define TWO RCONST(2.0)
#define TWOTHIRDS RCONST(0.6666666666666667)

/*
 * ------------------------------------------------------------------
 * IDANICSLU interface functions
 * ------------------------------------------------------------------
 */

static int IDANICSLUInit(IDAMem IDA_mem) {
  IDASlsMem idasls_mem;

  idasls_mem = (IDASlsMem)IDA_mem->ida_lmem;
  idasls_mem->s_solver_data = (SNicsLU *)malloc(sizeof(SNicsLU));

  NicsLU_Initialize(idasls_mem->s_solver_data);

  idasls_mem->s_nje = 0;
  idasls_mem->s_first_factorize = 1;

  idasls_mem->s_last_flag = 0;
  return (0);
}

static int IDANICSLUSetup(IDAMem IDA_mem, N_Vector yyp, N_Vector ypp,
                          N_Vector rrp, N_Vector tmp1, N_Vector tmp2,
                          N_Vector tmp3) {
  static int count = 0;
  int retval;
  realtype tn, cj;
  IDASlsMem idasls_mem;
  IDASlsSparseJacFn jaceval;
  SNicsLU *nicslu_data;
  SlsMat JacMat;
  void *jacdata;
  static int (*performFactorization)(SNicsLU *) = NULL;
  static int (*performReFactorization)(SNicsLU *, real__t *) = NULL;

  realtype uround_twothirds;

  uround_twothirds = SUNRpowerR(IDA_mem->ida_uround, TWOTHIRDS);

  idasls_mem = (IDASlsMem)(IDA_mem->ida_lmem);
  tn = IDA_mem->ida_tn;
  cj = IDA_mem->ida_cj;

  nicslu_data = (SNicsLU *)idasls_mem->s_solver_data;

  jaceval = idasls_mem->s_jaceval;
  jacdata = idasls_mem->s_jacdata;
  JacMat = idasls_mem->s_JacMat;

  /* Check that Jacobian eval routine is set */
  if (jaceval == NULL) {
    IDAProcessError(IDA_mem, IDASLS_JAC_NOSET, "IDASSLS", "IDANICSLUSetup",
                    MSGSP_JAC_NOSET);
    free(idasls_mem);
    idasls_mem = NULL;
    return (IDASLS_JAC_NOSET);
  }

  //* Increment nje counter and call Jacobian eval routine. */
  idasls_mem->s_nje++;
  retval = jaceval(tn, cj, yyp, ypp, rrp, JacMat, jacdata, tmp1, tmp2, tmp3);

  if (retval < 0) {
    IDAProcessError(IDA_mem, IDASLS_JACFUNC_UNRECVR, "IDASSLS",
                    "IDANICSLUSetup", MSGSP_JACFUNC_FAILED);
    idasls_mem->s_last_flag = IDASLS_JACFUNC_UNRECVR;
    return (IDASLS_JACFUNC_UNRECVR);
  }
  if (retval > 0) {
    idasls_mem->s_last_flag = IDASLS_JACFUNC_RECVR;
    return (+1);
  }

  uint__t n = JacMat->N;
  uint__t nnz = JacMat->NNZ;
  uint__t *ai = JacMat->rowvals;
  uint__t *ap = JacMat->colptrs;
  real__t *ax = JacMat->data;
  int num;

  if (idasls_mem->s_first_factorize) {
    /* ------------------------------------------------------------
       Get the symbolic factorization
       ------------------------------------------------------------*/

    if (nicslu_data == NULL) {
      IDAProcessError(IDA_mem, IDASLS_JAC_NOSET, "IDASSLS", "IDANICSLUSetup",
                      MSGSP_JAC_NOSET);
      return (+1);
    }

    NicsLU_CreateMatrix(nicslu_data, n, nnz, ax, ai, ap);
    //nicslu_data->cfgf[0] = .9;
    nicslu_data->cfgi[0] = 1;

    int ret = NicsLU_Analyze(nicslu_data);
    char *numth = getenv("THREADS");
    if (numth != NULL && atoi(numth) > 1) {
      ret = NicsLU_CreateScheduler(nicslu_data);
      //printf("suggestion: %s.\n", ret == 0 ? "parallel" : "sequential");
      num = atoi(numth);
      ret = NicsLU_CreateThreads(nicslu_data, num, TRUE);
      ret = NicsLU_BindThreads(nicslu_data, FALSE);
      performFactorization = &NicsLU_Factorize_MT;
      performReFactorization = &NicsLU_ReFactorize_MT;
      /*if (ret == NICS_OK)
        printf("%d threads created\n", num);*/
    } else {
//	    printf("One thread created\n");
      performFactorization = &NicsLU_Factorize;
      performReFactorization = &NicsLU_ReFactorize;
      num = 1;
    }

    if (ret != NICS_OK) {
      IDAProcessError(IDA_mem, IDASLS_PACKAGE_FAIL, "IDASSLS", "IDANICSLUSetup",
                      MSGSP_PACKAGE_FAIL);
      return (IDASLS_PACKAGE_FAIL);
    }

    /* ------------------------------------------------------------
      Compute the LU factorization of  the Jacobian.
      ------------------------------------------------------------*/
    performFactorization(nicslu_data);
    idasls_mem->s_first_factorize = 0;
  } else {
    performReFactorization(nicslu_data, JacMat->data);

    /*-----------------------------------------------------------
      Check if a cheap estimate of the reciprocal of the condition
      number is getting too small.  If so, delete
      the prior numeric factorization and recompute it.
      -----------------------------------------------------------*/
    real__t cond;
    NicsLU_ConditionNumber(nicslu_data, &cond);

    /* Condition number may be getting large.
    Compute more accurate estimate */

    if ((cond) > 1./TWOTHIRDS) {
      /* More accurate estimate also says condition number is
         large, so recompute the numeric factorization */

      performFactorization(nicslu_data);
    }
  }

  idasls_mem->s_last_flag = IDASLS_SUCCESS;
  return (0);
}

static int IDANICSLUSolve(IDAMem IDA_mem, N_Vector b, N_Vector weight,
                          N_Vector ycur, N_Vector ypcur, N_Vector rrcur) {
  int flag;
  realtype cjratio;
  IDASlsMem idasls_mem;
  SNicsLU *nicslu_data;
  SlsMat JacMat;
  realtype *bd;

  idasls_mem = (IDASlsMem)IDA_mem->ida_lmem;
  JacMat = idasls_mem->s_JacMat;
  cjratio = IDA_mem->ida_cjratio;
  nicslu_data = (SNicsLU *)idasls_mem->s_solver_data;
  bd = N_VGetArrayPointer(b);

  /* Call NICSLU to solve the linear system */
  flag = NicsLU_Solve(nicslu_data, bd);

  if(flag != NICS_OK){
	 if(flag==NICSLU_ARGUMENT_ERROR){
		 printf("entweder nicslu_data oder bd NULL\n");
	 }
	 if(flag==NICSLU_MATRIX_NOT_FACTORIZED)
		 printf("Matrix nicht faktorisiert\n");
 	IDAProcessError(IDA_mem, IDASLS_PACKAGE_FAIL, "IDASLS", "IDANICSLUSolve", MSGSP_PACKAGE_FAIL);
	return MSGSP_PACKAGE_FAIL;	
  }

  /* Scale the correction to account for change in cj. */
  if (cjratio != ONE)
    N_VScale(TWO / (ONE + cjratio), b, b);

  idasls_mem->s_last_flag = IDASLS_SUCCESS;
  return (IDASLS_SUCCESS);
}

static int IDANICSLUFree(IDAMem IDA_mem) {
  IDASlsMem idasls_mem;
  SNicsLU *nicslu_data;

  idasls_mem = (IDASlsMem)IDA_mem->ida_lmem;
  nicslu_data = (SNicsLU *)idasls_mem->s_solver_data;

  NicsLU_Destroy(nicslu_data);
  free(nicslu_data);
  return (IDASLS_SUCCESS);
}

int IDANICSLUB(void *ida_mem, int which, int n, int nnz) {
  IDAMem IDA_mem;
  IDAadjMem IDAADJ_mem;
  IDABMem IDAB_mem;
  IDASlsMemB idaslsB_mem;
  void *ida_memB;
  int flag;

  /* Is ida_mem alright? */
  if (ida_mem == NULL) {
    IDAProcessError(NULL, IDASLS_MEM_NULL, "IDASSLS", "IDANICSLUB",
                    MSGSP_CAMEM_NULL);
    return (IDASLS_MEM_NULL);
  }
  IDA_mem = (IDAMem)ida_mem;

  /* Is ASA initialized? */
  if (IDA_mem->ida_adjMallocDone == FALSE) {
    IDAProcessError(IDA_mem, IDASLS_NO_ADJ, "IDASSLS", "IDANICSLUB",
                    MSGSP_NO_ADJ);
    return (IDASLS_NO_ADJ);
  }
  IDAADJ_mem = IDA_mem->ida_adj_mem;

  /* Check the value of which */
  if (which >= IDAADJ_mem->ia_nbckpbs) {
    IDAProcessError(IDA_mem, IDASLS_ILL_INPUT, "IDASSLS", "IDANICSLUB",
                    MSGSP_BAD_WHICH);
    return (IDASLS_ILL_INPUT);
  }

  /* Find the IDABMem entry in the linked list corresponding to 'which'. */
  IDAB_mem = IDAADJ_mem->IDAB_mem;
  while (IDAB_mem != NULL) {
    if (which == IDAB_mem->ida_index)
      break;
    /* advance */
    IDAB_mem = IDAB_mem->ida_next;
  }

  /* Alloc memory for IDASlsMemRecB */
  idaslsB_mem = (IDASlsMemB)malloc(sizeof(struct IDASlsMemRecB));
  if (idaslsB_mem == NULL) {
    IDAProcessError(IDAB_mem->IDA_mem, IDASLS_MEM_FAIL, "IDASSLS", "IDANICSLUB",
                    MSGSP_MEM_FAIL);
    return (IDASLS_MEM_FAIL);
  }

  /* set matrix type and initialize Jacob function. */
  idaslsB_mem->s_djacB = NULL;

  /* Attach lmemB data and lfreeB function. */
  IDAB_mem->ida_lmem = idaslsB_mem;
  IDAB_mem->ida_lfree = IDANICSLUFreeB;

  /* Call IDANICSLU to the IDAS data of the backward problem. */
  ida_memB = (void *)IDAB_mem->IDA_mem;
  flag = IDANICSLU(ida_memB, n, nnz);

  if (flag != IDASLS_SUCCESS) {
    free(idaslsB_mem);
    idaslsB_mem = NULL;
  }

  return (flag);
}

int IDANICSLUReInit(void *ida_mem_v, int n, int nnz, int reinit_type) {
  IDAMem ida_mem;
  IDASlsMem idasls_mem;
  SNicsLU *nicslu_data;
  SlsMat JacMat;

  /* Return immediately if ida_mem is NULL. */
  if (ida_mem_v == NULL) {
    IDAProcessError(NULL, IDASLS_MEM_NULL, "IDASSLS", "IDANICSLUReInit",
                    MSGSP_IDAMEM_NULL);
    return (IDASLS_MEM_NULL);
  }
  ida_mem = (IDAMem)ida_mem_v;

  /* Return immediately if ark_lmem is NULL. */
  if (ida_mem->ida_lmem == NULL) {
    IDAProcessError(NULL, IDASLS_LMEM_NULL, "IDASSLS", "IDANICSLUReInit",
                    MSGSP_LMEM_NULL);
    return (IDASLS_LMEM_NULL);
  }

  idasls_mem = (IDASlsMem)(ida_mem->ida_lmem);
  nicslu_data = (SNicsLU *)idasls_mem->s_solver_data;

  /* Return if reinit_type is not valid */
  if ((reinit_type != 1) && (reinit_type != 2)) {
    IDAProcessError(NULL, IDASLS_ILL_INPUT, "IDASSLS", "IDANICSLUReInit",
                    MSGSP_ILL_INPUT);
    return (IDASLS_ILL_INPUT);
  }

  JacMat = idasls_mem->s_JacMat;

  if (reinit_type == 1) {

    /* Destroy previous Jacobian information */
    if (idasls_mem->s_JacMat) {
      DestroySparseMat(idasls_mem->s_JacMat);
    }

    /* Allocate memory for the sparse Jacobian */
    idasls_mem->s_JacMat = NewSparseMat(n, n, nnz);
    if (idasls_mem->s_JacMat == NULL) {
      IDAProcessError(ida_mem, IDASLS_MEM_FAIL, "IDASSLS", "IDANICSLU",
                      MSGSP_MEM_FAIL);
      return (IDASLS_MEM_FAIL);
    }
  }

  /* Free the prior factorazation and reset for first factorization */
  NicsLU_Destroy(nicslu_data);
  idasls_mem->s_first_factorize = 1;

  idasls_mem->s_last_flag = IDASLS_SUCCESS;

  return (0);
}

int IDANICSLU(void *ida_mem, int n, int nnz) {
  IDAMem IDA_mem;
  IDASlsMem idasls_mem;
  SNicsLU *nicslu_data;
  int flag;

  if (ida_mem == NULL) {
    IDAProcessError(NULL, IDASLS_MEM_NULL, "IDASSLS", "IDANICSLU",
                    MSGSP_IDAMEM_NULL);
    return (IDASLS_MEM_NULL);
  }

  IDA_mem = (IDAMem)ida_mem;

  /* Test if the NVECTOR package is compatible with the Direct solver */
  if (IDA_mem->ida_tempv1->ops->nvgetarraypointer == NULL) {
    IDAProcessError(IDA_mem, IDASLS_ILL_INPUT, "IDASSLS", "IDANICLSU",
                    MSGSP_BAD_NVECTOR);
    return (IDASLS_ILL_INPUT);
  }

  if (IDA_mem->ida_lfree != NULL)
    flag = IDA_mem->ida_lfree(IDA_mem);

  /* Set five main function fields in IDA_mem. */
  IDA_mem->ida_linit = IDANICSLUInit;
  IDA_mem->ida_lsetup = IDANICSLUSetup;
  IDA_mem->ida_lsolve = IDANICSLUSolve;
  IDA_mem->ida_lperf = NULL;
  IDA_mem->ida_lfree = IDANICSLUFree;

  idasls_mem = (IDASlsMem)malloc(sizeof(struct IDASlsMemRec));
  if (idasls_mem == NULL) {
    IDAProcessError(IDA_mem, IDASLS_MEM_FAIL, "IDASSLS", "IDANICSLU",
                    MSGSP_MEM_FAIL);
    return (IDASLS_MEM_FAIL);
  }

  nicslu_data = (SNicsLU *)malloc(sizeof(struct tagSNicsLU));
  if (nicslu_data == NULL) {
    IDAProcessError(IDA_mem, IDASLS_MEM_FAIL, "IDASSLS", "IDANICSLU",
                    MSGSP_MEM_FAIL);
    return (IDASLS_MEM_FAIL);
  }
  IDA_mem->ida_setupNonNull = TRUE;

  /* Set default Jacobian routine and Jacobian data */
  idasls_mem->s_jaceval = NULL;
  idasls_mem->s_jacdata = IDA_mem->ida_user_data;

  /* Allocate memory for the sparse Jacobian */
  idasls_mem->s_JacMat = NewSparseMat(n, n, nnz);
  if (idasls_mem->s_JacMat == NULL) {
    IDAProcessError(IDA_mem, IDASLS_MEM_FAIL, "IDASSLS", "IDANICSLU",
                    MSGSP_MEM_FAIL);
    return (IDASLS_MEM_FAIL);
  }

  /* ToDo */
  // SET UP NICSLU STRUCTURES!!!
  IDA_mem->ida_lmem = idasls_mem;

  return (IDASLS_SUCCESS);
}

int IDANICSLUReInitB(void *ida_mem, int which, int n, int nnz,
                     int reinit_type) {
  IDAMem IDA_mem;
  IDAadjMem IDAADJ_mem;
  IDABMem IDAB_mem;
  IDASlsMemB idaslsB_mem;
  void *ida_memB;
  int flag;

  /* Is ida_mem allright? */
  if (ida_mem == NULL) {
    IDAProcessError(NULL, IDASLS_MEM_NULL, "IDASSLS", "IDANICSLUReInitB",
                    MSGSP_CAMEM_NULL);
    return (IDASLS_MEM_NULL);
  }
  IDA_mem = (IDAMem)ida_mem;

  /* Is ASA initialized? */
  if (IDA_mem->ida_adjMallocDone == FALSE) {
    IDAProcessError(IDA_mem, IDASLS_NO_ADJ, "IDASSLS", "IDANICSLUReInitB",
                    MSGSP_NO_ADJ);
    return (IDASLS_NO_ADJ);
  }
  IDAADJ_mem = IDA_mem->ida_adj_mem;

  /* Check the value of which */
  if (which >= IDAADJ_mem->ia_nbckpbs) {
    IDAProcessError(IDA_mem, IDASLS_ILL_INPUT, "IDASSLS", "IDANICSLUReInitB",
                    MSGSP_BAD_WHICH);
    return (IDASLS_ILL_INPUT);
  }

  /* Find the IDABMem entry in the linked list corresponding to 'which'. */
  IDAB_mem = IDAADJ_mem->IDAB_mem;
  while (IDAB_mem != NULL) {
    if (which == IDAB_mem->ida_index)
      break;
    /* advance */
    IDAB_mem = IDAB_mem->ida_next;
  }

  ida_memB = (void *)(IDAB_mem->IDA_mem);

  flag = IDANICSLUReInit(ida_memB, n, nnz, reinit_type);

  return (flag);
}

static void IDANICSLUFreeB(IDABMem IDAB_mem) {
  IDASlsMemB idaslsB_mem;

  idaslsB_mem = (IDASlsMemB)IDAB_mem->ida_lmem;

  free(idaslsB_mem);
}
