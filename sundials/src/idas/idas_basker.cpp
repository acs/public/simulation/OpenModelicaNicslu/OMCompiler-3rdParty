#include <stdio.h>
#include <stdlib.h>

#include "idas/idas_basker.h"
#include "idas_impl.h"
#include "idas_sparse_impl.h"
#include "sundials/sundials_math.h"
#include "sundials/sundials_basker_impl.h"
static int IDABASKERInit(IDAMem IDA_mem);

static int IDABASKERSetup(IDAMem IDA_mem, N_Vector yyp, N_Vector ypp,
                          N_Vector rrp, N_Vector tmp1, N_Vector tmp2,
                          N_Vector tmp3);

static int IDABASKERSolve(IDAMem IDA_mem, N_Vector b, N_Vector weight,
                          N_Vector ycur, N_Vector ypcur, N_Vector rrcur);

static int IDABASKERFree(IDAMem IDA_mem);

/* IDABASKER lfreeB function */

static void IDABASKERFreeB(IDABMem IDAB_mem);

#define ONE RCONST(1.0)
#define TWO RCONST(2.0)
#define TWOTHIRDS RCONST(0.6666666666666667)

#ifdef BASKER_KOKKOS
 typedef Kokkos::OpenMP Exe_Space;
#else
 typedef void* Exe_Space;
#endif

typedef double Entry;
typedef int Int;

/*
 * ------------------------------------------------------------------
 * IDABASKER interface functions
 * ------------------------------------------------------------------
 */

static int IDABASKERInit(IDAMem IDA_mem) {
  IDASlsMem idasls_mem;

  idasls_mem = (IDASlsMem)IDA_mem->ida_lmem;
  //idasls_mem->s_solver_data = (BaskerNS::Basker<Int,Entry,Exe_Space> *)malloc(sizeof(BaskerNS::Basker<int,Entry,Exe_Space>));
  idasls_mem->s_solver_data = new BaskerNS::Basker<Int,Entry,Exe_Space>();
  BaskerNS::Basker<Int,Entry,Exe_Space>* basker_data = (BaskerNS::Basker<Int,Entry,Exe_Space>*) idasls_mem->s_solver_data;
   
  basker_data->Options.no_pivot=false;
  basker_data->Options.symmetric=false;
  basker_data->Options.realloc=true;
  basker_data->Options.btf=true;
  //basker_data->Options.verbose=true;
  //basker_data->Options.transpose=false;
  idasls_mem->s_nje = 0;
  idasls_mem->s_first_factorize = 1;

  idasls_mem->s_last_flag = 0;
  return (0);
}

static int IDABASKERSetup(IDAMem IDA_mem, N_Vector yyp, N_Vector ypp,
                          N_Vector rrp, N_Vector tmp1, N_Vector tmp2,
                          N_Vector tmp3) {
  static int count = 0;
  int retval;
  realtype tn, cj;
  IDASlsMem idasls_mem;
  IDASlsSparseJacFn jaceval;
  BaskerNS::Basker<Int,Entry,Exe_Space> *basker_data;

  SlsMat JacMat;
  void *jacdata;

  realtype uround_twothirds;

  uround_twothirds = SUNRpowerR(IDA_mem->ida_uround, TWOTHIRDS);

  idasls_mem = (IDASlsMem)(IDA_mem->ida_lmem);
  tn = IDA_mem->ida_tn;
  cj = IDA_mem->ida_cj;

  basker_data = (BaskerNS::Basker<Int,Entry,Exe_Space> *)idasls_mem->s_solver_data;

  jaceval = idasls_mem->s_jaceval;
  jacdata = idasls_mem->s_jacdata;
  JacMat = idasls_mem->s_JacMat;

  /* Check that Jacobian eval routine is set */
  if (jaceval == NULL) {
    IDAProcessError(IDA_mem, IDASLS_JAC_NOSET, "IDASSLS", "IDABASKERSetup",
                    MSGSP_JAC_NOSET);
    free(idasls_mem);
    idasls_mem = NULL;
    return (IDASLS_JAC_NOSET);
  }

  /* Increment nje counter and call Jacobian eval routine. */
  idasls_mem->s_nje++;
  retval = jaceval(tn, cj, yyp, ypp, rrp, JacMat, jacdata, tmp1, tmp2, tmp3);

  if (retval < 0) {
    IDAProcessError(IDA_mem, IDASLS_JACFUNC_UNRECVR, "IDASSLS",
                    "IDABASKERSetup", MSGSP_JACFUNC_FAILED);
    idasls_mem->s_last_flag = IDASLS_JACFUNC_UNRECVR;
    return (IDASLS_JACFUNC_UNRECVR);
  }
  if (retval > 0) {
    idasls_mem->s_last_flag = IDASLS_JACFUNC_RECVR;
    return (+1);
  }

  Int n = (Int) JacMat->N;
  Int m = (Int) JacMat->M;
  Int nnz = (Int) JacMat->NNZ;
  Int *row_idx = (Int*) JacMat->rowvals;
  Int *col_ptr = (Int*) JacMat->colptrs;
  Entry *vals = (Entry *) JacMat->data;
  Int numthreads=1;
  bool transpose=false;

  basker_data->SetThreads(numthreads);
  if(idasls_mem->s_first_factorize==1){
  	basker_data->Symbolic(m,n,nnz,col_ptr,row_idx,vals,transpose);
	idasls_mem->s_first_factorize=0;
  }
  basker_data->Factor(m,n,nnz,col_ptr,row_idx,vals);
  
  idasls_mem->s_last_flag = IDASLS_SUCCESS;
  return (0);
}

static int IDABASKERSolve(IDAMem IDA_mem, N_Vector b, N_Vector weight,
                          N_Vector ycur, N_Vector ypcur, N_Vector rrcur) {
  int flag;
  realtype cjratio;
  IDASlsMem idasls_mem;
  BaskerNS::Basker<Int,Entry,Exe_Space> *basker_data;
  SlsMat JacMat;
  realtype *bd;

  idasls_mem = (IDASlsMem)IDA_mem->ida_lmem;
  JacMat = idasls_mem->s_JacMat;
  cjratio = IDA_mem->ida_cjratio;
  basker_data = (BaskerNS::Basker<Int,Entry,Exe_Space> *)idasls_mem->s_solver_data;
  bd = N_VGetArrayPointer(b);

  /* Call BASKER to solve the linear system */
  //TODO: call solve properly
  basker_data->Solve(1,bd,bd);

  /* Scale the correction to account for change in cj. */
  if (cjratio != ONE)
    N_VScale(TWO / (ONE + cjratio), b, b);

  idasls_mem->s_last_flag = IDASLS_SUCCESS;
  return (IDASLS_SUCCESS);
}

static int IDABASKERFree(IDAMem IDA_mem) {
  IDASlsMem idasls_mem;
  BaskerNS::Basker<Int,Entry,Exe_Space> *basker_data;

  idasls_mem = (IDASlsMem)IDA_mem->ida_lmem;
  basker_data = (BaskerNS::Basker<Int,Entry,Exe_Space> *)idasls_mem->s_solver_data;
  
  basker_data->Finalize();  
  delete idasls_mem->s_solver_data;
  Kokkos::finalize();
  return (IDASLS_SUCCESS);
}

int IDABASKERB(void *ida_mem, int which, int n, int nnz) {
  IDAMem IDA_mem;
  IDAadjMem IDAADJ_mem;
  IDABMem IDAB_mem;
  IDASlsMemB idaslsB_mem;
  void *ida_memB;
  int flag;

  /* Is ida_mem alright? */
  if (ida_mem == NULL) {
    IDAProcessError(NULL, IDASLS_MEM_NULL, "IDASSLS", "IDABASKERB",
                    MSGSP_CAMEM_NULL);
    return (IDASLS_MEM_NULL);
  }
  IDA_mem = (IDAMem)ida_mem;

  /* Is ASA initialized? */
  if (IDA_mem->ida_adjMallocDone == FALSE) {
    IDAProcessError(IDA_mem, IDASLS_NO_ADJ, "IDASSLS", "IDABASKERB",
                    MSGSP_NO_ADJ);
    return (IDASLS_NO_ADJ);
  }
  IDAADJ_mem = IDA_mem->ida_adj_mem;

  /* Check the value of which */
  if (which >= IDAADJ_mem->ia_nbckpbs) {
    IDAProcessError(IDA_mem, IDASLS_ILL_INPUT, "IDASSLS", "IDABASKERB",
                    MSGSP_BAD_WHICH);
    return (IDASLS_ILL_INPUT);
  }

  /* Find the IDABMem entry in the linked list corresponding to 'which'. */
  IDAB_mem = IDAADJ_mem->IDAB_mem;
  while (IDAB_mem != NULL) {
    if (which == IDAB_mem->ida_index)
      break;
    /* advance */
    IDAB_mem = IDAB_mem->ida_next;
  }

  /* Alloc memory for IDASlsMemRecB */
  idaslsB_mem = (IDASlsMemB)malloc(sizeof(struct IDASlsMemRecB));
  if (idaslsB_mem == NULL) {
    IDAProcessError(IDAB_mem->IDA_mem, IDASLS_MEM_FAIL, "IDASSLS", "IDABASKERB",
                    MSGSP_MEM_FAIL);
    return (IDASLS_MEM_FAIL);
  }

  /* set matrix type and initialize Jacob function. */
  idaslsB_mem->s_djacB = NULL;

  /* Attach lmemB data and lfreeB function. */
  IDAB_mem->ida_lmem = idaslsB_mem;
  IDAB_mem->ida_lfree = IDABASKERFreeB;

  /* Call IDABASKER to the IDAS data of the backward problem. */
  ida_memB = (void *)IDAB_mem->IDA_mem;
  flag = IDABASKER(ida_memB, n, nnz);

  if (flag != IDASLS_SUCCESS) {
    free(idaslsB_mem);
    idaslsB_mem = NULL;
  }

  return (flag);
}

int IDABASKERReInit(void *ida_mem_v, int n, int nnz, int reinit_type) {
  IDAMem ida_mem;
  IDASlsMem idasls_mem;
  BaskerNS::Basker<Int,Entry,Exe_Space> *basker_data;
  SlsMat JacMat;

  /* Return immediately if ida_mem is NULL. */
  if (ida_mem_v == NULL) {
    IDAProcessError(NULL, IDASLS_MEM_NULL, "IDASSLS", "IDABASKERReInit",
                    MSGSP_IDAMEM_NULL);
    return (IDASLS_MEM_NULL);
  }
  ida_mem = (IDAMem)ida_mem_v;

  /* Return immediately if ark_lmem is NULL. */
  if (ida_mem->ida_lmem == NULL) {
    IDAProcessError(NULL, IDASLS_LMEM_NULL, "IDASSLS", "IDABASKERReInit",
                    MSGSP_LMEM_NULL);
    return (IDASLS_LMEM_NULL);
  }

  idasls_mem = (IDASlsMem)(ida_mem->ida_lmem);
  basker_data = (BaskerNS::Basker<Int,Entry,Exe_Space> *)idasls_mem->s_solver_data;

  /* Return if reinit_type is not valid */
  if ((reinit_type != 1) && (reinit_type != 2)) {
    IDAProcessError(NULL, IDASLS_ILL_INPUT, "IDASSLS", "IDABASKERReInit",
                    MSGSP_ILL_INPUT);
    return (IDASLS_ILL_INPUT);
  }

  JacMat = idasls_mem->s_JacMat;

  if (reinit_type == 1) {

    /* Destroy previous Jacobian information */
    if (idasls_mem->s_JacMat) {
      DestroySparseMat(idasls_mem->s_JacMat);
    }

    /* Allocate memory for the sparse Jacobian */
    idasls_mem->s_JacMat = NewSparseMat(n, n, nnz);
    if (idasls_mem->s_JacMat == NULL) {
      IDAProcessError(ida_mem, IDASLS_MEM_FAIL, "IDASSLS", "IDABASKER",
                      MSGSP_MEM_FAIL);
      return (IDASLS_MEM_FAIL);
    }
  }

  /* Free the prior factorazation and reset for first factorization */
  basker_data->Finalize();
  idasls_mem->s_first_factorize = 1;

  idasls_mem->s_last_flag = IDASLS_SUCCESS;

  return (0);
}

int IDABASKER(void *ida_mem, int n, int nnz) {
  IDAMem IDA_mem;
  IDASlsMem idasls_mem;
  BaskerNS::Basker<Int,Entry,Exe_Space> *basker_data;
  int flag;

  if (ida_mem == NULL) {
    IDAProcessError(NULL, IDASLS_MEM_NULL, "IDASSLS", "IDABASKER",
                    MSGSP_IDAMEM_NULL);
    return (IDASLS_MEM_NULL);
  }

  IDA_mem = (IDAMem)ida_mem;

  /* Test if the NVECTOR package is compatible with the Direct solver */
  if (IDA_mem->ida_tempv1->ops->nvgetarraypointer == NULL) {
    IDAProcessError(IDA_mem, IDASLS_ILL_INPUT, "IDASSLS", "IDANICLSU",
                    MSGSP_BAD_NVECTOR);
    return (IDASLS_ILL_INPUT);
  }

  if (IDA_mem->ida_lfree != NULL)
    flag = IDA_mem->ida_lfree(IDA_mem);

  /* Set five main function fields in IDA_mem. */
  IDA_mem->ida_linit = IDABASKERInit;
  IDA_mem->ida_lsetup = IDABASKERSetup;
  IDA_mem->ida_lsolve = IDABASKERSolve;
  IDA_mem->ida_lperf = NULL;
  IDA_mem->ida_lfree = IDABASKERFree;

  idasls_mem = (IDASlsMem)malloc(sizeof(struct IDASlsMemRec));
  if (idasls_mem == NULL) {
    IDAProcessError(IDA_mem, IDASLS_MEM_FAIL, "IDASSLS", "IDABASKER",
                    MSGSP_MEM_FAIL);
    return (IDASLS_MEM_FAIL);
  }

  basker_data = (BaskerNS::Basker<Int,Entry,Exe_Space> *)malloc(sizeof(BaskerNS::Basker<Int,Entry,Exe_Space>));
  if (basker_data == NULL) {
    IDAProcessError(IDA_mem, IDASLS_MEM_FAIL, "IDASSLS", "IDABASKER",
                    MSGSP_MEM_FAIL);
    return (IDASLS_MEM_FAIL);
  }
  IDA_mem->ida_setupNonNull = TRUE;

  /* Set default Jacobian routine and Jacobian data */
  idasls_mem->s_jaceval = NULL;
  idasls_mem->s_jacdata = IDA_mem->ida_user_data;

  /* Allocate memory for the sparse Jacobian */
  idasls_mem->s_JacMat = NewSparseMat(n, n, nnz);
  if (idasls_mem->s_JacMat == NULL) {
    IDAProcessError(IDA_mem, IDASLS_MEM_FAIL, "IDASSLS", "IDABASKER",
                    MSGSP_MEM_FAIL);
    return (IDASLS_MEM_FAIL);
  }

  /* ToDo */
  // SET UP BASKER STRUCTURES!!!
  IDA_mem->ida_lmem = idasls_mem;
  Kokkos::InitArguments init_args;
  init_args.num_threads=1;
  Kokkos::initialize(init_args);

  #ifdef BASKER_KOKKOS
  printf("hwloc aval: %ld\n numa count: %ld\n thrd numa: %ld\n", Kokkos::hwloc::available(), Kokkos::hwloc::get_available_numa_count(), Kokkos::hwloc::get_available_cores_per_numa());
 #endif


  return (IDASLS_SUCCESS);
}

int IDABASKERReInitB(void *ida_mem, int which, int n, int nnz,
                     int reinit_type) {
  IDAMem IDA_mem;
  IDAadjMem IDAADJ_mem;
  IDABMem IDAB_mem;
  IDASlsMemB idaslsB_mem;
  void *ida_memB;
  int flag;

  /* Is ida_mem allright? */
  if (ida_mem == NULL) {
    IDAProcessError(NULL, IDASLS_MEM_NULL, "IDASSLS", "IDABASKERReInitB",
                    MSGSP_CAMEM_NULL);
    return (IDASLS_MEM_NULL);
  }
  IDA_mem = (IDAMem)ida_mem;

  /* Is ASA initialized? */
  if (IDA_mem->ida_adjMallocDone == FALSE) {
    IDAProcessError(IDA_mem, IDASLS_NO_ADJ, "IDASSLS", "IDABASKERReInitB",
                    MSGSP_NO_ADJ);
    return (IDASLS_NO_ADJ);
  }
  IDAADJ_mem = IDA_mem->ida_adj_mem;

  /* Check the value of which */
  if (which >= IDAADJ_mem->ia_nbckpbs) {
    IDAProcessError(IDA_mem, IDASLS_ILL_INPUT, "IDASSLS", "IDABASKERReInitB",
                    MSGSP_BAD_WHICH);
    return (IDASLS_ILL_INPUT);
  }

  /* Find the IDABMem entry in the linked list corresponding to 'which'. */
  IDAB_mem = IDAADJ_mem->IDAB_mem;
  while (IDAB_mem != NULL) {
    if (which == IDAB_mem->ida_index)
      break;
    /* advance */
    IDAB_mem = IDAB_mem->ida_next;
  }

  ida_memB = (void *)(IDAB_mem->IDA_mem);

  flag = IDABASKERReInit(ida_memB, n, nnz, reinit_type);

  return (flag);
}

static void IDABASKERFreeB(IDABMem IDAB_mem) {
  IDASlsMemB idaslsB_mem;

  idaslsB_mem = (IDASlsMemB)IDAB_mem->ida_lmem;

  free(idaslsB_mem);
}
