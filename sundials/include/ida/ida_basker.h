/* 
 * --------------------------------------------------------
 *  $Revision: 1 $
 *  $Date: 2019-03-11 11:50 +0100 (Mon, 11 Mar 2019) $
 *  -------------------------------------------------------
 *  Programmer(s) : Lennart M. Schumacher @ ACS
 *  ------------------------------------------------------
 *  This is the header file for the IDABASKER linear solver module
 * */

#ifndef _IDABASKER_H
#define _IDABASKER_H

#include "ida/ida_sparse.h"
#include "sundials/sundials_sparse.h"

#ifdef __cplusplus /* wrapper to enable C++ usage */
extern "C" {
#endif	

/* ------------------------------------------------------
 * Function : IDABASKER
 * ------------------------------------------------------
 */
SUNDIALS_EXPORT int IDABASKER(void* ida_mem, int n, int nnz);

/* ------------------------------------------------------
 * Function : IDABASKERReInit
 * ------------------------------------------------------
 */
SUNDIALS_EXPORT int IDABASKERReInit(void *ida_mem_v, int n, int nnz, int reinit_type);

/* ------------------------------------------------------
 * Function : IDABASKERB
 * ------------------------------------------------------
 */
SUNDIALS_EXPORT int IDABASKERB(void* ida_mem, int which, int nB, int nnzB);

/* ------------------------------------------------------
 * Optional Input Specification Functions
 * ------------------------------------------------------
 * INSERT HERE SETTHREADS! ToDo
 */
//SUNDIALS_EXPORT int IDABASKERSetOrdering(void* ida_mem, int ordering_choice);

#ifdef __cplusplus
}
#endif

#endif
