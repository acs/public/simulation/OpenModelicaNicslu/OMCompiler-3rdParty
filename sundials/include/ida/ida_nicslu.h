/* 
 * --------------------------------------------------------
 *  $Revision: 1 $
 *  $Date: 2018-03-21 13:13 +0100 (Wed, 21 Mar 2018) $
 *  -------------------------------------------------------
 *  Programmer(s) : Lennart M. Schumacher @ RTE
 *  ------------------------------------------------------
 *  This is the header file for the IDANICSLU linear solver module
 * */

#ifndef _IDANICSLU_H
#define _IDANICSLU_H

#include "ida/ida_sparse.h"
#include "sundials/sundials_sparse.h"

#ifdef __cplusplus /* wrapper to enable C++ usage */
extern "C" {
#endif	

/* ------------------------------------------------------
 * Function : IDANICSLU
 * ------------------------------------------------------
 */
SUNDIALS_EXPORT int IDANICSLU(void* ida_mem, int n, int nnz);

/* ------------------------------------------------------
 * Function : IDANICSLUReInit
 * ------------------------------------------------------
 */
SUNDIALS_EXPORT int IDANICSLUReInit(void *ida_mem_v, int n, int nnz, int reinit_type);

/* ------------------------------------------------------
 * Function : IDANICSLUB
 * ------------------------------------------------------
 */
SUNDIALS_EXPORT int IDANICSLUB(void* ida_mem, int which, int nB, int nnzB);

/* ------------------------------------------------------
 * Optional Input Specification Functions
 * ------------------------------------------------------
 * INSERT HERE SETTHREADS! ToDo
 */
//SUNDIALS_EXPORT int IDANICSLUSetOrdering(void* ida_mem, int ordering_choice);

#ifdef __cplusplus
}
#endif

#endif
