/*
 * -----------------------------------------------------------------
 * $Revision: 1 $
 * $Date: 2018-04-05 13:21:31 +0100 (Thu, 05 Apr 2018) $
 * -----------------------------------------------------------------
 * Programmer(s): Lennart M. Schumacher @ RTE
 * -----------------------------------------------------------------
 * LLNS Copyright Start
 * Copyright (c) 2014, Lawrence Livermore National Security
 * This work was performed under the auspices of the U.S. Department
 * of Energy by Lawrence Livermore National Laboratory in part under
 * Contract W-7405-Eng-48 and in part under Contract DE-AC52-07NA27344.
 * Produced at the Lawrence Livermore National Laboratory.
 * All rights reserved.
 * For details, see the LICENSE file.
 * LLNS Copyright End
 * -----------------------------------------------------------------
 * This is the header file for the KINNICSLU linear solver module.
 * -----------------------------------------------------------------
 */

#ifndef _KINNICSLU_H
#define _KINNICSLU_H

#include "kinsol/kinsol_sparse.h"
#include "sundials/sundials_sparse.h"

#ifdef __cplusplus  /* wrapper to enable C++ usage */
extern "C" {
#endif

/*
 * -----------------------------------------------------------------
 * Function : KINNICSLU
 * -----------------------------------------------------------------
 * A call to the KINNICSLU function links the main integrator
 * with the KINNICSLU linear solver module.
 *
 * kin_mem is the pointer to integrator memory returned by
 *     KINCreate.
 *
 *
 * KINNICSLU returns:
 *     KINSLU_SUCCESS   = 0  if successful
 *     KINSLU_LMEM_FAIL = -1 if there was a memory allocation failure
 *     KINSLU_ILL_INPUT = -2 if NVECTOR found incompatible
 *
 * NOTE: The NICSLU linear solver assumes a serial implementation
 *       of the NVECTOR package. Therefore, KINKLU will first
 *       test for a compatible N_Vector internal representation
 *       by checking that the functions N_VGetArrayPointer and
 *       N_VSetArrayPointer exist.
 * -----------------------------------------------------------------
 */

  SUNDIALS_EXPORT int kinNICSLU(void *kin_mem, int n, int nnz);

/*
 * -----------------------------------------------------------------
 * Function : KINNICSLUReInit
 * -----------------------------------------------------------------
 * This routine reinitializes memory and flags for a new factorization
 * (symbolic and numeric) to be conducted at the next solver setup
 * call.  This routine is useful in the cases where the number of nonzeroes
 * has changed or if the structure of the linear system has changed
 * which would require a new symbolic (and numeric factorization).
 *
 * The reinit_type argumenmt governs the level of reinitialization:
 *
 * reinit_type = 1: The Jacobian matrix will be destroyed and
 *                  a new one will be allocated based on the nnz
 *                  value passed to this call. New symbolic and
 *                  numeric factorizations will be completed at the next
 *                  solver setup.
 *
 * reinit_type = 2: Only symbolic and numeric factorizations will be
 *                  completed.  It is assumed that the Jacobian size
 *                  has not exceeded the size of nnz given in the prior
 *                  call to KINKLU.
 *
 * This routine assumes no other changes to solver use are necessary.
 *
 * The return value is KINSLS_SUCCESS = 0, KINSLS_MEM_NULL = -1,
 * KINSLS_LMEM_NULL = -2, KINSLS_ILL_INPUT = -3, or KINSLS_MEM_FAIL = -4.
 * -----------------------------------------------------------------
 */

  SUNDIALS_EXPORT int kinNICSLUReInit(void *kin_mem, int n, int nnz,
                   int reinit_type);




#ifdef __cplusplus
}
#endif

#endif
