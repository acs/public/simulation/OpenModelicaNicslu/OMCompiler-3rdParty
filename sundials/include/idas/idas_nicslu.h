/*
 * --------------------------------------------------------
 *  $Revision: 1 $
 *  $Date: 2018-03-21 13:13 +0100 (Wed, 21 Mar 2018) $
 *  -------------------------------------------------------
 *  Programmer(s) : Lennart M. Schumacher @ RTE
 *  ------------------------------------------------------
 *  This is the header file for the IDANICSLU linear solver module
 * */

#ifndef _IDASNICSLU_H
#define _IDASNICSLU_H

#include "idas/idas_sparse.h"
#include "sundials/sundials_sparse.h"

#ifdef __cplusplus /* wrapper to enable C++ usage */
extern "C" {
#endif

/* ------------------------------------------------------
 * Function : IDANICSLU
 * ------------------------------------------------------
 */
SUNDIALS_EXPORT int IDANICSLU(void* ida_mem, int n, int nnz);

/* ------------------------------------------------------
 * Function : IDANICSLUReInit
 * ------------------------------------------------------
 */
SUNDIALS_EXPORT int IDANICSLUReInit(void *ida_mem_v, int n, int nnz, int reinit_type);

/* ------------------------------------------------------
 * Function : IDANICSLUB
 * ------------------------------------------------------
 */
SUNDIALS_EXPORT int IDANICSLUB(void* ida_mem, int which, int nB, int nnzB);

SUNDIALS_EXPORT int IDANICSLUReInitB(void *ida_mem, int which, int nB, int nnzB, int reinit_type);


/* ------------------------------------------------------
 * Optional Input Specification Functions
 * ------------------------------------------------------
 */
//SUNDIALS_EXPORT int IDANICSLUSetOrderingB(void* ida_mem, int ordering_choice);

#ifdef __cplusplus
}
#endif

#endif
