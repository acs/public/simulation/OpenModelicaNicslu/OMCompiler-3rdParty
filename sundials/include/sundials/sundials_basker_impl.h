/*
 * -----------------------------------------------------------------
 * $Revision: 2 $
 * $Date: 2019-03-11 11:50 +0100 (Mon, 11 Mar 2019) $
 * ----------------------------------------------------------------- 
 * Programmer(s): Lennart M. Schumacher @ ACS
 * -----------------------------------------------------------------
 * Implementation header file for the Sundials interface to 
 * the Basker linear solver.
 * -----------------------------------------------------------------
 */

#ifndef _SUNBASKER_IMPL_H
#define _SUNBASKER_IMPL_H

#ifndef _S_BASKER_H
#define _S_BASKER_H
// TODO: find files to inclue in this header (shylubasker_def.hpp?)
//#include ".h"

#include "shylubasker_decl.hpp"
#include "shylubasker_def.hpp"
#ifdef BASKER_KOKKOS
#include <Kokkos_Core.hpp>
#else
#include <omp.h>
#endif // BASKER_KOKKOS
#endif

#ifdef __cplusplus  /* wrapper to enable C++ usage */
extern "C" {
#endif

/*
 * -----------------------------------------------------------------
 * Definition of BASKERData
 * -----------------------------------------------------------------
 */
 
typedef struct BASKERDataRec {
 
  /* Structure for Basker-specific data */
 
} *BASKERData;
 
#ifdef __cplusplus
} 
#endif 
 
#endif 

