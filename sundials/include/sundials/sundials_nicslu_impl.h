/*
 * -----------------------------------------------------------------
 * $Revision: 1 $
 * $Date: 2018-03-21 13:31 +0100 (Wed, 21 Mar 2018) $
 * ----------------------------------------------------------------- 
 * Programmer(s): Lennart M. Schumacher @ RTE
 * -----------------------------------------------------------------
 * Implementation header file for the Sundials interface to 
 * the NICSLU linear solver.
 * -----------------------------------------------------------------
 */

#ifndef _SUNNICSLU_IMPL_H
#define _SUNNICSLU_IMPL_H

#ifndef _S_NICSLU_H
#define _S_NICSLU_H
#include "nicslu.h"
#endif

#ifdef __cplusplus  /* wrapper to enable C++ usage */
extern "C" {
#endif

/*
 * -----------------------------------------------------------------
 * Definition of NICSLUData
 * Not needed -> nicslu has its own structure
 * -----------------------------------------------------------------
 */
 
typedef struct NICSLUDataRec {
 
  /* Structure for NICSLU-specific data */
 
} *NICSLUData;
 
#ifdef __cplusplus
} 
#endif 
 
#endif 

