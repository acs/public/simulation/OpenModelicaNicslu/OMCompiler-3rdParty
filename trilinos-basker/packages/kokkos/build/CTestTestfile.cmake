# CMake generated Testfile for 
# Source directory: /opt/measurementEnvironment/3rdParty/kokkos
# Build directory: /opt/measurementEnvironment/3rdParty/kokkos/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("core")
subdirs("containers")
subdirs("algorithms")
