# Install script for directory: /opt/measurementEnvironment/3rdParty/kokkos

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/Kokkos" TYPE FILE FILES "/opt/measurementEnvironment/3rdParty/kokkos/build/kokkos_generated_settings.cmake")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/kokkos_generated_settings.cmake")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`" TYPE FILE FILES "/opt/measurementEnvironment/3rdParty/kokkos/build/kokkos_generated_settings.cmake")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/libkokkos.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/libkokkos.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/libkokkos.so"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/libkokkos.so")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib" TYPE SHARED_LIBRARY FILES "/opt/measurementEnvironment/3rdParty/kokkos/build/libkokkos.so")
  if(EXISTS "$ENV{DESTDIR}/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/libkokkos.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/libkokkos.so")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/libkokkos.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES
    "/opt/measurementEnvironment/3rdParty/kokkos/EXPORT"
    "/opt/measurementEnvironment/3rdParty/kokkos/KokkosTargets"
    "/opt/measurementEnvironment/3rdParty/kokkos/core/src/"
    FILES_MATCHING REGEX "/[^/]*\\.hpp$")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES
    "/opt/measurementEnvironment/3rdParty/kokkos/EXPORT"
    "/opt/measurementEnvironment/3rdParty/kokkos/KokkosTargets"
    "/opt/measurementEnvironment/3rdParty/kokkos/containers/src/"
    FILES_MATCHING REGEX "/[^/]*\\.hpp$")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES
    "/opt/measurementEnvironment/3rdParty/kokkos/EXPORT"
    "/opt/measurementEnvironment/3rdParty/kokkos/KokkosTargets"
    "/opt/measurementEnvironment/3rdParty/kokkos/algorithms/src/"
    FILES_MATCHING REGEX "/[^/]*\\.hpp$")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE FILE FILES "/opt/measurementEnvironment/3rdParty/kokkos/build/KokkosCore_config.h")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/CMake/Kokkos/KokkosConfig.cmake")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/CMake/Kokkos" TYPE FILE FILES "/opt/measurementEnvironment/3rdParty/kokkos/build/CMakeFiles/KokkosConfig.cmake")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/CMake/Kokkos/KokkosTargets.cmake")
    file(DIFFERENT EXPORT_FILE_CHANGED FILES
         "$ENV{DESTDIR}/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/CMake/Kokkos/KokkosTargets.cmake"
         "/opt/measurementEnvironment/3rdParty/kokkos/build/CMakeFiles/Export/_opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/CMake/Kokkos/KokkosTargets.cmake")
    if(EXPORT_FILE_CHANGED)
      file(GLOB OLD_CONFIG_FILES "$ENV{DESTDIR}/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/CMake/Kokkos/KokkosTargets-*.cmake")
      if(OLD_CONFIG_FILES)
        message(STATUS "Old export file \"$ENV{DESTDIR}/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/CMake/Kokkos/KokkosTargets.cmake\" will be replaced.  Removing files [${OLD_CONFIG_FILES}].")
        file(REMOVE ${OLD_CONFIG_FILES})
      endif()
    endif()
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/CMake/Kokkos/KokkosTargets.cmake")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/CMake/Kokkos" TYPE FILE FILES "/opt/measurementEnvironment/3rdParty/kokkos/build/CMakeFiles/Export/_opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/CMake/Kokkos/KokkosTargets.cmake")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^()$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/CMake/Kokkos/KokkosTargets-noconfig.cmake")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/CMake/Kokkos" TYPE FILE FILES "/opt/measurementEnvironment/3rdParty/kokkos/build/CMakeFiles/Export/_opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/CMake/Kokkos/KokkosTargets-noconfig.cmake")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/opt/measurementEnvironment/3rdParty/kokkos/build/kokkos.pc")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/opt/measurementEnvironment/3rdParty/kokkos/build/core/cmake_install.cmake")
  include("/opt/measurementEnvironment/3rdParty/kokkos/build/containers/cmake_install.cmake")
  include("/opt/measurementEnvironment/3rdParty/kokkos/build/algorithms/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/opt/measurementEnvironment/3rdParty/kokkos/build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
