#----------------------------------------------------------------
# Generated CMake target import file.
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "kokkos" for configuration ""
set_property(TARGET kokkos APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(kokkos PROPERTIES
  IMPORTED_LOCATION_NOCONFIG "/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/libkokkos.so"
  IMPORTED_SONAME_NOCONFIG "libkokkos.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS kokkos )
list(APPEND _IMPORT_CHECK_FILES_FOR_kokkos "/opt/measurementEnvironment/3rdParty/kokkos/build/`pwd`/lib/libkokkos.so" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
