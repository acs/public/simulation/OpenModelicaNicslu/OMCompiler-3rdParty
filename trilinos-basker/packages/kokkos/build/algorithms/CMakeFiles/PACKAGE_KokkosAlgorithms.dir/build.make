# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.14

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /opt/measurementEnvironment/3rdParty/kokkos

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /opt/measurementEnvironment/3rdParty/kokkos/build

# Include any dependencies generated for this target.
include algorithms/CMakeFiles/PACKAGE_KokkosAlgorithms.dir/depend.make

# Include the progress variables for this target.
include algorithms/CMakeFiles/PACKAGE_KokkosAlgorithms.dir/progress.make

# Include the compile flags for this target's objects.
include algorithms/CMakeFiles/PACKAGE_KokkosAlgorithms.dir/flags.make

algorithms/CMakeFiles/PACKAGE_KokkosAlgorithms.dir/dummy.cpp.o: algorithms/CMakeFiles/PACKAGE_KokkosAlgorithms.dir/flags.make
algorithms/CMakeFiles/PACKAGE_KokkosAlgorithms.dir/dummy.cpp.o: algorithms/dummy.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/opt/measurementEnvironment/3rdParty/kokkos/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object algorithms/CMakeFiles/PACKAGE_KokkosAlgorithms.dir/dummy.cpp.o"
	cd /opt/measurementEnvironment/3rdParty/kokkos/build/algorithms && /usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/PACKAGE_KokkosAlgorithms.dir/dummy.cpp.o -c /opt/measurementEnvironment/3rdParty/kokkos/build/algorithms/dummy.cpp

algorithms/CMakeFiles/PACKAGE_KokkosAlgorithms.dir/dummy.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/PACKAGE_KokkosAlgorithms.dir/dummy.cpp.i"
	cd /opt/measurementEnvironment/3rdParty/kokkos/build/algorithms && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /opt/measurementEnvironment/3rdParty/kokkos/build/algorithms/dummy.cpp > CMakeFiles/PACKAGE_KokkosAlgorithms.dir/dummy.cpp.i

algorithms/CMakeFiles/PACKAGE_KokkosAlgorithms.dir/dummy.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/PACKAGE_KokkosAlgorithms.dir/dummy.cpp.s"
	cd /opt/measurementEnvironment/3rdParty/kokkos/build/algorithms && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /opt/measurementEnvironment/3rdParty/kokkos/build/algorithms/dummy.cpp -o CMakeFiles/PACKAGE_KokkosAlgorithms.dir/dummy.cpp.s

# Object files for target PACKAGE_KokkosAlgorithms
PACKAGE_KokkosAlgorithms_OBJECTS = \
"CMakeFiles/PACKAGE_KokkosAlgorithms.dir/dummy.cpp.o"

# External object files for target PACKAGE_KokkosAlgorithms
PACKAGE_KokkosAlgorithms_EXTERNAL_OBJECTS =

algorithms/libPACKAGE_KokkosAlgorithms.a: algorithms/CMakeFiles/PACKAGE_KokkosAlgorithms.dir/dummy.cpp.o
algorithms/libPACKAGE_KokkosAlgorithms.a: algorithms/CMakeFiles/PACKAGE_KokkosAlgorithms.dir/build.make
algorithms/libPACKAGE_KokkosAlgorithms.a: algorithms/CMakeFiles/PACKAGE_KokkosAlgorithms.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/opt/measurementEnvironment/3rdParty/kokkos/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX static library libPACKAGE_KokkosAlgorithms.a"
	cd /opt/measurementEnvironment/3rdParty/kokkos/build/algorithms && $(CMAKE_COMMAND) -P CMakeFiles/PACKAGE_KokkosAlgorithms.dir/cmake_clean_target.cmake
	cd /opt/measurementEnvironment/3rdParty/kokkos/build/algorithms && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/PACKAGE_KokkosAlgorithms.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
algorithms/CMakeFiles/PACKAGE_KokkosAlgorithms.dir/build: algorithms/libPACKAGE_KokkosAlgorithms.a

.PHONY : algorithms/CMakeFiles/PACKAGE_KokkosAlgorithms.dir/build

algorithms/CMakeFiles/PACKAGE_KokkosAlgorithms.dir/clean:
	cd /opt/measurementEnvironment/3rdParty/kokkos/build/algorithms && $(CMAKE_COMMAND) -P CMakeFiles/PACKAGE_KokkosAlgorithms.dir/cmake_clean.cmake
.PHONY : algorithms/CMakeFiles/PACKAGE_KokkosAlgorithms.dir/clean

algorithms/CMakeFiles/PACKAGE_KokkosAlgorithms.dir/depend:
	cd /opt/measurementEnvironment/3rdParty/kokkos/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /opt/measurementEnvironment/3rdParty/kokkos /opt/measurementEnvironment/3rdParty/kokkos/algorithms /opt/measurementEnvironment/3rdParty/kokkos/build /opt/measurementEnvironment/3rdParty/kokkos/build/algorithms /opt/measurementEnvironment/3rdParty/kokkos/build/algorithms/CMakeFiles/PACKAGE_KokkosAlgorithms.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : algorithms/CMakeFiles/PACKAGE_KokkosAlgorithms.dir/depend

